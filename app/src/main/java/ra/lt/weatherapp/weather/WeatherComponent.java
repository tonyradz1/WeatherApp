package ra.lt.weatherapp.weather;

import dagger.Subcomponent;
import ra.lt.weatherapp.utils.date.DateModule;
import ra.lt.weatherapp.utils.location.LocationModule;
import ra.lt.weatherapp.utils.messageshower.MessageShowerModule;
import ra.lt.weatherapp.utils.permission.PermissionModule;
import ra.lt.weatherapp.utils.settings.SettingsModule;

@Subcomponent(modules = {
        SettingsModule.class,
        WeatherModule.class,
        MessageShowerModule.class,
        PermissionModule.class,
        LocationModule.class,
        DateModule.class
})
public interface WeatherComponent {
    void inject(WeatherFragment weatherFragment);
}