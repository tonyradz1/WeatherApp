package ra.lt.weatherapp.weather;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import ra.lt.weatherapp.api.WeatherService;
import ra.lt.weatherapp.utils.date.DateFormatter;
import ra.lt.weatherapp.utils.di.ActivityContext;
import ra.lt.weatherapp.utils.location.LocationHelper;
import ra.lt.weatherapp.utils.location.LocationService;
import ra.lt.weatherapp.utils.messageshower.MessageShower;
import ra.lt.weatherapp.utils.messageshower.SnackbarMessage;
import ra.lt.weatherapp.utils.network.ConnectionChecker;
import ra.lt.weatherapp.utils.permission.PermissionHelper;
import ra.lt.weatherapp.utils.rx.UiScheduler;

@Module
public class WeatherModule {
    private WeatherFragment weatherFragment;

    public WeatherModule(WeatherFragment weatherFragment) {
        this.weatherFragment = weatherFragment;
    }

    @Provides
    public WeatherFragment weatherFragment() {
        return weatherFragment;
    }

    @Provides
    @ActivityContext
    public Context context() {
        return weatherFragment.getActivity();
    }

    @Provides
    public WeatherModel weatherModel(WeatherService weatherService,
                                     LocationService locationService,
                                     WeatherRepository weatherRepository) {
        return new WeatherModelImpl(weatherService, locationService, weatherRepository);
    }

    @Provides
    public WeatherPresenter weatherPresenter(WeatherModel weatherModel,
                                             ConnectionChecker connectionChecker,
                                             LocationHelper locationHelper,
                                             PermissionHelper permissionHelper,
                                             @SnackbarMessage MessageShower toastShower,
                                             @UiScheduler Scheduler scheduler,
                                             DateFormatter defaultDateFormatter) {
        return new WeatherPresenterImpl(weatherModel, connectionChecker, locationHelper,
                toastShower, permissionHelper, defaultDateFormatter, scheduler);
    }
}