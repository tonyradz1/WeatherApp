package ra.lt.weatherapp.weather;

import android.location.Location;

import java.util.List;

import io.reactivex.Single;
import ra.lt.weatherapp.BuildConfig;
import ra.lt.weatherapp.api.WeatherService;
import ra.lt.weatherapp.api.response.WeatherResponse;
import ra.lt.weatherapp.utils.location.LocationService;

public class WeatherModelImpl implements WeatherModel {
    private static final String API_KEY = BuildConfig.WEATHER_API_KEY;
    private static final String UNITS = "metric";
    private WeatherService weatherService;
    private LocationService locationService;
    private WeatherRepository weatherRepository;

    public WeatherModelImpl(WeatherService weatherService, LocationService locationService,
                            WeatherRepository weatherRepository) {
        this.weatherService = weatherService;
        this.locationService = locationService;
        this.weatherRepository = weatherRepository;
    }

    @Override
    public Single<Location> getLocation() {
        return locationService.getLocation();
    }

    @Override
    public Single<WeatherResponse> fetchWeather(Location location) {
        return weatherService.getWeather(API_KEY, UNITS,
                location.getLongitude(), location.getLatitude());
    }

    @Override
    public Single<List<Weather>> saveWeather(List<Weather> weatherList) {
        return weatherRepository.saveWeather(weatherList);
    }

    @Override
    public Single<Weather> getSavedWeather() {
        return weatherRepository.getCurrentWeather();
    }
}
