package ra.lt.weatherapp.weather;

import android.support.annotation.StringRes;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

import ra.lt.weatherapp.R;
import ra.lt.weatherapp.api.response.ResponseWeather;
import ra.lt.weatherapp.api.response.WeatherDetails;
import ra.lt.weatherapp.utils.metrics.MetricsUtil;
import ra.lt.weatherapp.utils.url.UrlGeneratorUtil;

public class Weather {
    public static final String COLUMN_NAME_ID = "_id";
    public static final String COLUMN_NAME_CURRENT_CONDITION = "current_condition";
    public static final String COLUMN_NAME_TEMPERATURE = "temperature";
    public static final String COLUMN_NAME_WIND_SPEED = "wind_speed";
    public static final String COLUMN_NAME_WIND_DIRECTION = "wind_direction";
    public static final String COLUMN_NAME_ICON_URL = "icon_url";
    public static final String COLUMN_NAME_WEATHER_DATE = "weather_date";
    public static final String COLUMN_NAME_FETCH_DATE = "fetch_date";

    @DatabaseField(columnName = COLUMN_NAME_ID, generatedId = true)
    public long id;

    @DatabaseField(columnName = COLUMN_NAME_CURRENT_CONDITION)
    private String currentCondition;

    @DatabaseField(columnName = COLUMN_NAME_TEMPERATURE)
    private int temperature;

    @DatabaseField(columnName = COLUMN_NAME_WIND_SPEED)
    private int windSpeedMph;

    @DatabaseField(columnName = COLUMN_NAME_WIND_DIRECTION)
    private int windDirection;

    @DatabaseField(columnName = COLUMN_NAME_ICON_URL)
    private String iconUrl;

    @DatabaseField(columnName = COLUMN_NAME_WEATHER_DATE)
    private Date weatherDate;

    @DatabaseField(columnName = COLUMN_NAME_FETCH_DATE)
    private Date fetchDate;

    public Weather() {
        // Empty
    }

    public Weather(ResponseWeather responseWeather) {
        WeatherDetails weatherDetails = responseWeather.weatherDetails.get(0);
        if (weatherDetails != null) {
            currentCondition = weatherDetails.description;
            currentCondition = currentCondition.toUpperCase().charAt(0) +
                    currentCondition.substring(1, currentCondition.length());
            iconUrl = UrlGeneratorUtil.generateIconUrl(weatherDetails.icon);
        }

        float convertedSpeed = MetricsUtil.convertKmsToMiles(responseWeather.wind.speed);
        windSpeedMph = Math.round(convertedSpeed);
        temperature = Math.round(responseWeather.mainWeatherDetails.temperature);
        windDirection = convertDegreeToDirection(responseWeather.wind.degree);
        weatherDate = new Date(responseWeather.weatherDate * 1000);
        fetchDate = new Date(System.currentTimeMillis());
    }

    public String getCurrentCondition() {
        return currentCondition;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getWindSpeedMph() {
        return windSpeedMph;
    }

    @StringRes
    public int getWindDirection() {
        return windDirection;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public Date getFetchDate() {
        return fetchDate;
    }

    private int convertDegreeToDirection(double degree) {
        int directions[] = {
                R.string.north,
                R.string.north_east,
                R.string.east,
                R.string.south_east,
                R.string.south,
                R.string.south_west,
                R.string.west,
                R.string.north_west,
                R.string.north};

        return directions[(int) Math.round(((degree % 360) / 45))];
    }
}
