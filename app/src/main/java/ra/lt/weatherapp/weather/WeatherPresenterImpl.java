package ra.lt.weatherapp.weather;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.StringRes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import ra.lt.weatherapp.R;
import ra.lt.weatherapp.api.response.ResponseWeather;
import ra.lt.weatherapp.api.response.WeatherResponse;
import ra.lt.weatherapp.utils.date.DateFormatter;
import ra.lt.weatherapp.utils.location.LocationHelper;
import ra.lt.weatherapp.utils.messageshower.MessageShower;
import ra.lt.weatherapp.utils.mvp.BasePresenterImpl;
import ra.lt.weatherapp.utils.network.ConnectionChecker;
import ra.lt.weatherapp.utils.network.ConnectionRetryListener;
import ra.lt.weatherapp.utils.permission.PermissionHelper;

public class WeatherPresenterImpl extends BasePresenterImpl<WeatherView> implements
        WeatherPresenter, ConnectionRetryListener {
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 123;
    private WeatherModel model;
    private ConnectionChecker connectionChecker;
    private LocationHelper locationHelper;
    private MessageShower messageShower;
    private PermissionHelper permissionHelper;
    private DateFormatter dateFormatter;
    private Scheduler mainThreadScheduler;
    private boolean isOfflineDataLoaded = false;

    public WeatherPresenterImpl(WeatherModel model, ConnectionChecker connectionChecker,
                                LocationHelper locationHelper, MessageShower messageShower,
                                PermissionHelper permissionHelper, DateFormatter dateFormatter,
                                Scheduler mainThreadScheduler) {
        this.model = model;
        this.connectionChecker = connectionChecker;
        this.locationHelper = locationHelper;
        this.messageShower = messageShower;
        this.permissionHelper = permissionHelper;
        this.dateFormatter = dateFormatter;
        this.mainThreadScheduler = mainThreadScheduler;
    }

    @Override
    public void onLoad() {
        if (permissionHelper.isLocationPermissionGranted()) {
            if (connectionChecker.isOnline()) {
                if (locationHelper.isLocationEnabled()) {
                    onLocationEnabled();
                } else {
                    if (hasView()) {
                        getView().checkLocationSettings();
                    }
                }
            } else {
                showOfflineWeather();
            }
        } else {
            if (hasView()) {
                getView().askLocationPermission(REQUEST_CODE_LOCATION_PERMISSION);
            }
        }
    }

    @Override
    public void onRefreshClicked() {
        onLoad();
    }

    @Override
    public void onPermissionsResult(int requestCode, int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onLoad();
            } else {
                if (hasView()) {
                    messageShower.showShort(R.string.location_permission_error);
                }
            }
        }
    }

    @Override
    public void onLocationEnabled() {
        showProgressBar();
        model.getLocation()
                .observeOn(mainThreadScheduler)
                .subscribe(new SingleObserver<Location>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        // Empty
                    }

                    @Override
                    public void onSuccess(Location location) {
                        fetchWeather(location);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        onGettingLocationError();
                        hideProgressBar();
                    }
                });
    }

    @Override
    public void onReconnectClicked() {
        hideProgressBar();
        onLoad();
    }

    private void onGettingLocationError() {
        if (hasView()) {
            messageShower.showShort(R.string.location_receive_error);
        }
    }

    private void fetchWeather(Location location) {
        if (connectionChecker.isOnline()) {
            model.fetchWeather(location)
                    .map(new Function<WeatherResponse, List<Weather>>() {
                        @Override
                        public List<Weather> apply(WeatherResponse weatherResponse) {
                            return mapWeather(weatherResponse);
                        }
                    })
                    .observeOn(mainThreadScheduler)
                    .subscribe(new SingleObserver<List<Weather>>() {
                                   @Override
                                   public void onSubscribe(Disposable disposable) {
                                       // Empty
                                   }

                                   @Override
                                   public void onSuccess(List<Weather> weatherList) {
                                       onWeatherFetched(weatherList);
                                       hideProgressBar();
                                   }

                                   @Override
                                   public void onError(Throwable throwable) {
                                       showOfflineWeather();
                                       showErrorMessage(R.string.weather_fetch_error);
                                       hideProgressBar();
                                   }
                               }
                    );
        } else {
            showOfflineWeather();
        }
    }

    private void onWeatherFetched(List<Weather> weatherList) {
        if (!weatherList.isEmpty()) {
            handleOfflineDataIndicator();
            showWeather(weatherList.get(0));
            saveWeather(weatherList);
        }
    }

    private List<Weather> mapWeather(WeatherResponse weatherResponse) {
        List<Weather> weatherList = new ArrayList<>();
        List<ResponseWeather> responseWeatherList = weatherResponse.responseWeatherList;

        if (responseWeatherList != null && !responseWeatherList.isEmpty()) {
            for (ResponseWeather responseWeather : responseWeatherList) {
                weatherList.add(new Weather(responseWeather));
            }
        }

        return weatherList;
    }

    private void saveWeather(List<Weather> weatherList) {
        model.saveWeather(weatherList)
                .observeOn(mainThreadScheduler)
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        onOfflineDataStoringError();
                    }
                })
                .subscribe();
    }

    private void showOfflineWeather() {
        model.getSavedWeather()
                .observeOn(mainThreadScheduler)
                .subscribe(new SingleObserver<Weather>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        // Empty
                    }

                    @Override
                    public void onSuccess(Weather weather) {
                        onOfflineDataFetched(weather);
                        hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        showErrorMessage(R.string.no_stored_offline_data_error);
                        hideProgressBar();
                    }
                });
    }

    private void onOfflineDataStoringError() {
        if (hasView()) {
            messageShower.showShort(R.string.offline_data_storing_error);
        }
    }

    private void onOfflineDataFetched(Weather weather) {
        isOfflineDataLoaded = true;
        showWeather(weather);
        showLastUpdateDate(weather.getFetchDate());
        showErrorMessage(R.string.no_connection_error);
    }

    private void handleOfflineDataIndicator() {
        if (isOfflineDataLoaded) {
            isOfflineDataLoaded = false;
            if (hasView()) {
                getView().hideOfflineDataIndicator();
            }
        }
    }

    private void showLastUpdateDate(Date fetchDate) {
        if (hasView()) {
            getView().showLastUpdateDate(dateFormatter.formatDate(fetchDate));
        }
    }

    private void showErrorMessage(@StringRes int message) {
        if (hasView()) {
            messageShower.showNoConnection(message, R.string.retry, this);
        }
    }

    private void showWeather(Weather weather) {
        if (hasView()) {
            getView().showWeather(weather);
        }
    }

    private void showProgressBar() {
        if (hasView()) {
            getView().showProgressBar();
        }
    }

    private void hideProgressBar() {
        if (hasView()) {
            getView().hideProgressBar();
        }
    }
}
