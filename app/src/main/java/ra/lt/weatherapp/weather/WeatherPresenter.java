package ra.lt.weatherapp.weather;

import ra.lt.weatherapp.utils.mvp.BasePresenter;

public interface WeatherPresenter extends BasePresenter<WeatherView> {
    void onLoad();

    void onRefreshClicked();

    void onPermissionsResult(int requestCode, int[] grantResults);

    void onLocationEnabled();
}
