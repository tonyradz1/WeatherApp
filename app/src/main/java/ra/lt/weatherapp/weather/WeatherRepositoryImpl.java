package ra.lt.weatherapp.weather;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import ra.lt.weatherapp.utils.repository.BaseRepository;
import ra.lt.weatherapp.utils.repository.BaseRepositoryHelper;

public class WeatherRepositoryImpl extends BaseRepository implements WeatherRepository {
    private Dao<Weather, Long> weatherDao;
    private static final long PERIOD_BETWEEN_FORECAST = 3;
    private static final long DAY = 86400000L;
    private static final long HOUR = 3600000L;

    public WeatherRepositoryImpl(BaseRepositoryHelper helper, Scheduler singleThreadScheduler) {
        super(helper, singleThreadScheduler);
    }

    private Dao<Weather, Long> getWeatherDao() throws SQLException {
        if (weatherDao == null) {
            weatherDao = helper.getDao(Weather.class);
        }
        return weatherDao;
    }

    @Override
    public Single<List<Weather>> saveWeather(final List<Weather> weatherList) {
        return execute(new Callable<List<Weather>>() {
            @Override
            public List<Weather> call() throws Exception {
                TableUtils.clearTable(getConnectionSource(), Weather.class);
                for (Weather weather : weatherList) {
                    getWeatherDao().create(weather);
                }
                return weatherList;
            }
        });
    }

    @Override
    public Single<Weather> getCurrentWeather() {
        return execute(new Callable<Weather>() {
            @Override
            public Weather call() throws Exception {
                final Date dateNow = new Date();
                final Date dateOldest = new Date(dateNow.getTime() - DAY);
                final Date dateBefore = new Date(dateNow.getTime()
                        - PERIOD_BETWEEN_FORECAST * HOUR);
                final Date dateAfter = new Date(dateNow.getTime() + PERIOD_BETWEEN_FORECAST * HOUR);

                final QueryBuilder<Weather, Long> builder = getWeatherDao().queryBuilder();
                builder.orderBy(Weather.COLUMN_NAME_WEATHER_DATE, false)
                        .where()
                        .between(Weather.COLUMN_NAME_WEATHER_DATE, dateBefore, dateAfter)
                        .and()
                        .between(Weather.COLUMN_NAME_FETCH_DATE, dateOldest, dateNow);

                return builder.queryForFirst();
            }
        });
    }
}
