package ra.lt.weatherapp.weather;

import java.util.List;

import io.reactivex.Single;

public interface WeatherRepository {
    Single<List<Weather>> saveWeather(List<Weather> responseWeatherList);

    Single<Weather> getCurrentWeather();
}
