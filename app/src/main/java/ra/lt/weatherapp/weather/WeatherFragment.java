package ra.lt.weatherapp.weather;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ra.lt.weatherapp.R;
import ra.lt.weatherapp.RefreshButtonProvider;
import ra.lt.weatherapp.application.WeatherApplication;
import ra.lt.weatherapp.utils.imageloader.ImageLoader;
import ra.lt.weatherapp.utils.settings.LocationSettingsTask;

import static android.app.Activity.RESULT_OK;

public class WeatherFragment extends Fragment implements WeatherView {
    private static final String EMPTY = "";
    private static final int REQUEST_CODE_CHECK_LOCATION_SETTINGS = 5456;

    @BindView(R.id.currentWeatherConditionTextView)
    protected TextView currentWeatherConditionTextView;

    @BindView(R.id.temperatureTextView)
    protected TextView temperatureTextView;

    @BindView(R.id.windSpeedTextView)
    protected TextView windSpeedTextView;

    @BindView(R.id.windDirectionTextView)
    protected TextView windDirectionTextView;

    @BindView(R.id.lastUpdatedTextView)
    protected TextView lastUpdatedTextView;

    @BindView(R.id.weatherIconImageView)
    protected ImageView weatherIconImageView;

    @Inject
    protected WeatherPresenter presenter;

    @Inject
    protected ImageLoader imageLoader;

    @Inject
    @LocationSettingsTask
    protected Task<LocationSettingsResponse> locationSettingsTask;

    private Unbinder unbinder;
    private ProgressDialog dialog;

    public WeatherFragment() {
        // Empty
    }

    public static Fragment newInstance() {
        return new WeatherFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initDependencyInjection(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.takeView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getActivity() instanceof RefreshButtonProvider) {
            FloatingActionButton refreshButton =
                    ((RefreshButtonProvider) getActivity()).getRefreshButton();
            refreshButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onRefreshClicked();
                }
            });
        }

        presenter.onLoad();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        presenter.dropView(this);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHECK_LOCATION_SETTINGS && resultCode == RESULT_OK) {
            presenter.onLocationEnabled();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onPermissionsResult(requestCode, grantResults);
    }

    @Override
    public void askLocationPermission(int requestCode) {
        if (getActivity() != null) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
        }
    }

    @Override
    public void showWeather(Weather weather) {
        currentWeatherConditionTextView.setText(weather.getCurrentCondition());
        temperatureTextView.setText(
                getString(R.string.temperature_value, weather.getTemperature()));
        windSpeedTextView.setText(getString(R.string.wind_speed_value, weather.getWindSpeedMph()));
        windDirectionTextView.setText(weather.getWindDirection());
        imageLoader.loadImage(weatherIconImageView, weather.getIconUrl());
    }

    @Override
    public void showProgressBar() {
        dialog = ProgressDialog.show(getActivity(), EMPTY,
                getString(R.string.progress_loading), true);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void checkLocationSettings() {
        if (getActivity() != null) {
            locationSettingsTask.addOnSuccessListener(getActivity(),
                    new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            presenter.onLocationEnabled();
                        }
                    });

            locationSettingsTask.addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    if (exception instanceof ResolvableApiException) {
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            resolvable.startResolutionForResult(getActivity(),
                                    REQUEST_CODE_CHECK_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException ignore) {
                            // Empty
                        }
                    }
                }
            });
        }
    }

    @Override
    public void showLastUpdateDate(String date) {
        lastUpdatedTextView.setVisibility(View.VISIBLE);
        lastUpdatedTextView.setText(getString(R.string.last_updated_value, date));
    }

    @Override
    public void hideOfflineDataIndicator() {
        lastUpdatedTextView.setVisibility(View.GONE);
    }

    private void initDependencyInjection(Context context) {
        WeatherApplication.getComponent(context)
                .plus(new WeatherModule(this))
                .inject(this);
    }
}
