package ra.lt.weatherapp.weather;

public interface WeatherView {
    void askLocationPermission(int requestCode);

    void showWeather(Weather weather);

    void showProgressBar();

    void hideProgressBar();

    void checkLocationSettings();

    void showLastUpdateDate(String date);

    void hideOfflineDataIndicator();
}
