package ra.lt.weatherapp.weather;

import android.location.Location;

import java.util.List;

import io.reactivex.Single;
import ra.lt.weatherapp.api.response.WeatherResponse;

public interface WeatherModel {
    Single<Location> getLocation();

    Single<WeatherResponse> fetchWeather(Location location);

    Single<List<Weather>> saveWeather(List<Weather> weatherList);

    Single<Weather> getSavedWeather();
}
