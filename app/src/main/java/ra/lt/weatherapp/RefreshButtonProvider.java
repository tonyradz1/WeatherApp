package ra.lt.weatherapp;

import android.support.design.widget.FloatingActionButton;

public interface RefreshButtonProvider {
    FloatingActionButton getRefreshButton();
}
