package ra.lt.weatherapp.utils.repository;

import com.j256.ormlite.support.ConnectionSource;

import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public abstract class BaseRepository {
    protected final BaseRepositoryHelper helper;
    private Scheduler scheduler;

    public BaseRepository(BaseRepositoryHelper helper, Scheduler scheduler) {
        this.helper = helper;
        this.scheduler = scheduler;
    }

    public <T> Single<T> execute(Callable<T> callable) {
        return helper.execute(callable, scheduler);
    }

    public ConnectionSource getConnectionSource() {
        return helper.getConnectionSource();
    }
}