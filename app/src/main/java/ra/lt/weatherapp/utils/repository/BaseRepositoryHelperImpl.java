package ra.lt.weatherapp.utils.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import ra.lt.weatherapp.weather.Weather;

public class BaseRepositoryHelperImpl extends OrmLiteSqliteOpenHelper implements BaseRepositoryHelper {
    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;

    public BaseRepositoryHelperImpl(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Weather.class);
        } catch (SQLException cause) {
            Log.e(BaseRepositoryHelper.class.getSimpleName(), cause.toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        // Empty
    }

    public void onDelete(Context context) throws SQLException {
        context.deleteDatabase(context.getDatabasePath(DATABASE_NAME).getAbsolutePath());
    }

    @Override
    public <T> Single<T> execute(final Callable<T> callable, Scheduler singleThreadScheduler) {
        return Single.create(new SingleOnSubscribe<T>() {
            @Override
            public void subscribe(SingleEmitter<T> emitter) {
                try {
                    T result = TransactionManager.callInTransaction(
                            getConnectionSource(), callable);
                    emitter.onSuccess(result);
                } catch (SQLException error) {
                    emitter.onError(error);
                }
            }
        }).subscribeOn(singleThreadScheduler);
    }
}