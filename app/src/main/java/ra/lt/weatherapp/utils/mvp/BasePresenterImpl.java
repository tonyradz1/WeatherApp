package ra.lt.weatherapp.utils.mvp;

public class BasePresenterImpl<V> implements BasePresenter<V> {
    private V view;

    @Override
    public void takeView(V view) {
        this.view = view;
    }

    @Override
    public void dropView(V view) {
        this.view = null; // NOPMD
    }

    public V getView() {
        return view;
    }

    protected boolean hasView() {
        return view != null;
    }
}