package ra.lt.weatherapp.utils.location;

public interface LocationHelper {
    boolean isLocationEnabled();
}