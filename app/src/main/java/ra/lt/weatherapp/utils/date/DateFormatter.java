package ra.lt.weatherapp.utils.date;

import java.util.Date;

public interface DateFormatter {
    String formatDate(Date date);
}