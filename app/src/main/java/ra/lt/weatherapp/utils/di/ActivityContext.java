package ra.lt.weatherapp.utils.di;

import javax.inject.Qualifier;

@Qualifier
public @interface ActivityContext {
}
