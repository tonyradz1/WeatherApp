package ra.lt.weatherapp.utils.rx;

import javax.inject.Qualifier;

@Qualifier
public @interface UiScheduler {
}
