package ra.lt.weatherapp.utils.messageshower;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ra.lt.weatherapp.utils.di.ActivityContext;

@Module
public class MessageShowerModule {
    @Provides
    @SnackbarMessage
    public MessageShower snackbarMessageShower(@ActivityContext Context context) {
        return new SnackBarMessageShower(context);
    }
}
