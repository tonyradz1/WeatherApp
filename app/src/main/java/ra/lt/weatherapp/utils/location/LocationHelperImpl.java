package ra.lt.weatherapp.utils.location;

import android.location.LocationManager;

import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;

public class LocationHelperImpl implements LocationHelper {
    private LocationManager locationManager;

    public LocationHelperImpl(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    @Override
    public boolean isLocationEnabled() {
        boolean isGpsProviderEnabled = false;
        boolean isNetworkProviderEnabled = false;

        try {
            isGpsProviderEnabled = locationManager.isProviderEnabled(GPS_PROVIDER);
        } catch (Exception ignore) {
            // Empty
        }

        try {
            isNetworkProviderEnabled = locationManager.isProviderEnabled(NETWORK_PROVIDER);
        } catch (Exception ignore) {
            // Empty
        }

        return isGpsProviderEnabled || isNetworkProviderEnabled;
    }
}
