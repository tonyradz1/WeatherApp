package ra.lt.weatherapp.utils.metrics;

public class MetricsUtil {
    private static final float KM_TO_MI_PROPORTION = 0.621371f;

    public static float convertKmsToMiles(float km) {
        return KM_TO_MI_PROPORTION * km;
    }
}