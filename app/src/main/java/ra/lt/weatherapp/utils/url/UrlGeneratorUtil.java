package ra.lt.weatherapp.utils.url;

public class UrlGeneratorUtil {
    private static String IMAGES_END_POINT = "http://openweathermap.org/img/w/";
    private static String FILE_EXTENSION = ".png";

    public static String generateIconUrl(String iconName) {
        return IMAGES_END_POINT + iconName + FILE_EXTENSION;
    }
}