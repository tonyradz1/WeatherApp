package ra.lt.weatherapp.utils.location;

import android.location.Location;

import io.reactivex.Single;

public interface LocationService {
    Single<Location> getLocation();
}