package ra.lt.weatherapp.utils.imageloader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

public final class GlideImageLoader implements ImageLoader {
    private Context context;

    public GlideImageLoader(Context context) {
        this.context = context;
    }

    @Override
    public void loadImage(ImageView targetImageView, String url) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(targetImageView);
    }
}