package ra.lt.weatherapp.utils.permission;

import android.content.Context;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class PermissionHelperImpl implements PermissionHelper {
    private Context context;

    public PermissionHelperImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean isLocationPermissionGranted() {
        return checkSelfPermission(context, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED ||
                checkSelfPermission(context, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED;
    }
}
