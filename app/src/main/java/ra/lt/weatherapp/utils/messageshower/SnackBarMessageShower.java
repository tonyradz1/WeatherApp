package ra.lt.weatherapp.utils.messageshower;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;

import ra.lt.weatherapp.utils.network.ConnectionRetryListener;

import static android.support.design.widget.Snackbar.LENGTH_INDEFINITE;
import static android.support.design.widget.Snackbar.LENGTH_SHORT;

public class SnackBarMessageShower implements MessageShower {
    private final Context context;
    private Snackbar snackbar;
    private ViewGroup view;

    public SnackBarMessageShower(Context context) {
        this.context = context;
    }

    @Override
    public void showShort(@StringRes int message) {
        if (getView() != null) {
            snackbar = Snackbar.make(getView(), message, LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public void showNoConnection(@StringRes int message, @StringRes int buttonText,
                                 final ConnectionRetryListener listener) {
        if (getView() != null) {
            snackbar = Snackbar.make(getView(), message, LENGTH_INDEFINITE);
            snackbar.setAction(buttonText, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onReconnectClicked();
                }
            });
            snackbar.show();
        }
    }

    @Nullable
    private ViewGroup getView() {
        if (view != null) {
            return view;
        } else {
            Activity activity = (Activity) context;
            view = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content))
                    .getChildAt(0);
            return view;
        }
    }
}