package ra.lt.weatherapp.utils.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static java.lang.String.valueOf;

public class DefaultDateFormatter implements DateFormatter {
    private static final String DATE_FORMAT = "yyyy-MMM-dd HH:mm";

    @Override
    public String formatDate(Date date) {
        if (date == null) {
            return "";
        } else {
            DateFormat targetFormat = new SimpleDateFormat(DATE_FORMAT, Locale.UK);
            return valueOf(targetFormat.format(date));
        }
    }
}