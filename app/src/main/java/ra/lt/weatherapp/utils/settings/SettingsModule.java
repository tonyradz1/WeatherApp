package ra.lt.weatherapp.utils.settings;

import android.content.Context;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;

import dagger.Module;
import dagger.Provides;
import ra.lt.weatherapp.utils.di.ActivityContext;

@Module
public class SettingsModule {
    @Provides
    @LocationSettingsClient
    public SettingsClient locationSettingsClient(@ActivityContext Context context) {
        return LocationServices.getSettingsClient(context);
    }

    @Provides
    public LocationSettingsRequest locationSettingsRequest(LocationRequest locationRequest) {
        return new LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build();
    }

    @Provides
    @LocationSettingsTask
    public Task<LocationSettingsResponse> task(
            @LocationSettingsClient SettingsClient locationSettingsClient,
            LocationSettingsRequest locationSettingsRequest) {
        return locationSettingsClient.checkLocationSettings(locationSettingsRequest);
    }
}
