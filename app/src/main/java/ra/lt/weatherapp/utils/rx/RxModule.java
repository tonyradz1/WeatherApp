package ra.lt.weatherapp.utils.rx;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

@Module
public class RxModule {
    @Provides
    @Singleton
    public RxJava2CallAdapterFactory rxJava2CallAdapterFactory(
            @IoScheduler Scheduler scheduler) {
        return RxJava2CallAdapterFactory.createWithScheduler(scheduler);
    }

    @Provides
    @Singleton
    @IoScheduler
    Scheduler provideIOScheduler() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @UiScheduler
    Scheduler provideUIScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @SingleThreadScheduler
    Scheduler provideSingleThreadScheduler() {
        return Schedulers.from(Executors.newSingleThreadExecutor());
    }
}