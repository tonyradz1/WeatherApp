package ra.lt.weatherapp.utils.messageshower;

import android.support.annotation.StringRes;

import ra.lt.weatherapp.utils.network.ConnectionRetryListener;

public interface MessageShower {
    void showShort(@StringRes int message);

    void showNoConnection(@StringRes int message, @StringRes int buttonText,
                          ConnectionRetryListener listener);
}
