package ra.lt.weatherapp.utils.location;

import android.content.Context;
import android.location.LocationManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import ra.lt.weatherapp.utils.di.ActivityContext;
import ra.lt.weatherapp.utils.rx.IoScheduler;

@Module
public class LocationModule {
    private static final int NUMBER_OF_LOCATION_UPDATES = 1;
    private static final int FASTEST_UPDATE_INTERVAL = 500;

    @Provides
    public FusedLocationProviderClient fusedLocationProviderClient(
            @ActivityContext Context context) {
        return LocationServices.getFusedLocationProviderClient(context);
    }

    @Provides
    public LocationRequest locationRequest() {
        return new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setNumUpdates(NUMBER_OF_LOCATION_UPDATES)
                .setFastestInterval(FASTEST_UPDATE_INTERVAL)
                .setInterval(FASTEST_UPDATE_INTERVAL);
    }

    @Provides
    public LocationHelper locationHelper(@ActivityContext Context context) {
        LocationManager locationManager =
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return new LocationHelperImpl(locationManager);
    }

    @Provides
    public LocationService locationService(FusedLocationProviderClient fusedLocationProviderClient,
                                           LocationRequest locationRequest,
                                           @IoScheduler Scheduler scheduler) {
        return new LocationServiceImpl(fusedLocationProviderClient, locationRequest, scheduler);
    }
}
