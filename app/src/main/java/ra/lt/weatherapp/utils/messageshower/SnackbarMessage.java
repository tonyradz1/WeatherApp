package ra.lt.weatherapp.utils.messageshower;

import javax.inject.Qualifier;

@Qualifier
public @interface SnackbarMessage {
}
