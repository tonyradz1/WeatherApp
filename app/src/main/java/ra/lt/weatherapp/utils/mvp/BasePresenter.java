package ra.lt.weatherapp.utils.mvp;

public interface BasePresenter<V> {
    void takeView(V view);

    void dropView(V view);
}