package ra.lt.weatherapp.utils.repository;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public interface BaseRepositoryHelper {
    <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) throws SQLException;

    <T> Single<T> execute(Callable<T> callable, Scheduler scheduler);

    ConnectionSource getConnectionSource();
}