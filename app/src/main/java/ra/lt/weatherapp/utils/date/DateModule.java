package ra.lt.weatherapp.utils.date;

import dagger.Module;
import dagger.Provides;

@Module
public class DateModule {
    @Provides
    public DateFormatter defaultDateFormatter() {
        return new DefaultDateFormatter();
    }
}
