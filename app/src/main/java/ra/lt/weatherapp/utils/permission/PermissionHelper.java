package ra.lt.weatherapp.utils.permission;

public interface PermissionHelper {
    boolean isLocationPermissionGranted();
}