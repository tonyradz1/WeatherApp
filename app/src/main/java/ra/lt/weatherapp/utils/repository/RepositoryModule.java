package ra.lt.weatherapp.utils.repository;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import ra.lt.weatherapp.application.ApplicationContext;
import ra.lt.weatherapp.utils.rx.SingleThreadScheduler;
import ra.lt.weatherapp.weather.WeatherRepository;
import ra.lt.weatherapp.weather.WeatherRepositoryImpl;

@Module
public class RepositoryModule {
    @Provides
    @Singleton
    public BaseRepositoryHelper baseRepositoryHelper(@ApplicationContext Context context) {
        return new BaseRepositoryHelperImpl(context);
    }

    @Provides
    public WeatherRepository weatherRepository(BaseRepositoryHelper baseRepositoryHelper,
                                               @SingleThreadScheduler Scheduler scheduler) {
        return new WeatherRepositoryImpl(baseRepositoryHelper, scheduler);
    }
}
