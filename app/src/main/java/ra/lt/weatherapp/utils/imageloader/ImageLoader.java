package ra.lt.weatherapp.utils.imageloader;

import android.widget.ImageView;

public interface ImageLoader {
    void loadImage(ImageView target, String url);
}
