package ra.lt.weatherapp.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectionChecker {
    private final Context context;

    public ConnectionChecker(Context context) {
        this.context = context;
    }

    public boolean isOnline() {
        final ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null &&
                connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
