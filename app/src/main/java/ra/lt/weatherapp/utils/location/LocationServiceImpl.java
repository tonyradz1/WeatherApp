package ra.lt.weatherapp.utils.location;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

@SuppressLint("MissingPermission")
public class LocationServiceImpl implements LocationService {
    private static final int LOCATION_TIMEOUT = 30;
    private static final String LOCATION_ERROR = "Can't get current location";
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Scheduler ioScheduler;
    private LocationRequest locationRequest;

    public LocationServiceImpl(FusedLocationProviderClient fusedLocationProviderClient,
                               LocationRequest locationRequest, Scheduler ioScheduler) {
        this.fusedLocationProviderClient = fusedLocationProviderClient;
        this.ioScheduler = ioScheduler;
        this.locationRequest = locationRequest;
    }

    @Override
    public Single<Location> getLocation() {
        return Single.create(new SingleOnSubscribe<Location>() {
            @Override
            public void subscribe(final SingleEmitter<Location> emitter) {
                requestLastLocation(emitter);
            }
        })
                .timeout(LOCATION_TIMEOUT, TimeUnit.SECONDS)
                .subscribeOn(ioScheduler);
    }

    private void requestLastLocation(final SingleEmitter<Location> emitter) {
        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            emitter.onSuccess(location);
                        } else {
                            requestLocationUpdate(emitter);
                        }
                    }
                });
    }

    private void requestLocationUpdate(final SingleEmitter<Location> emitter) {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                if (location != null) {
                    emitter.onSuccess(location);
                } else {
                    emitter.onError(new RuntimeException(LOCATION_ERROR));
                }
                fusedLocationProviderClient.removeLocationUpdates(this);
            }
        }, Looper.getMainLooper());
    }
}
