package ra.lt.weatherapp.utils.permission;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ra.lt.weatherapp.utils.di.ActivityContext;

@Module
public class PermissionModule {
    @Provides
    public PermissionHelper permissionHelper(@ActivityContext Context context) {
        return new PermissionHelperImpl(context);
    }
}
