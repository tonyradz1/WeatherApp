package ra.lt.weatherapp.utils.imageloader;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ra.lt.weatherapp.application.ApplicationContext;

@Module
public class ImageLoaderModule {
    @Provides
    @Singleton
    public ImageLoader imageLoader(@ApplicationContext Context context) {
        return new GlideImageLoader(context);
    }
}
