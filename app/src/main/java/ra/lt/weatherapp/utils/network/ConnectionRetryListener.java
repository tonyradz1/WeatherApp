package ra.lt.weatherapp.utils.network;

public interface ConnectionRetryListener {
    void onReconnectClicked();
}