package ra.lt.weatherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ra.lt.weatherapp.weather.WeatherFragment;

public class MainActivity extends AppCompatActivity implements RefreshButtonProvider {
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.refreshFloatingActionButton)
    protected FloatingActionButton refreshFloatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupActionBar();

        if (savedInstanceState == null) {
            showWeatherFragment();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public FloatingActionButton getRefreshButton() {
        return refreshFloatingActionButton;
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setIcon(R.drawable.ic_cloud_white_24dp);
        }
    }

    private void showWeatherFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer, WeatherFragment.newInstance())
                .commit();
    }
}
