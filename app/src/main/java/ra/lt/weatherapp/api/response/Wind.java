package ra.lt.weatherapp.api.response;

import com.google.gson.annotations.SerializedName;

public class Wind {
    public Float speed;

    @SerializedName("deg")
    public Float degree;
}