package ra.lt.weatherapp.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseWeather {
    @SerializedName("dt")
    public long weatherDate;

    @SerializedName("weather")
    public List<WeatherDetails> weatherDetails;

    public Wind wind;

    @SerializedName("main")
    public MainWeatherDetails mainWeatherDetails;
}
