package ra.lt.weatherapp.api.response;

import com.google.gson.annotations.SerializedName;

public class MainWeatherDetails {
    @SerializedName("temp")
    public Float temperature;
}