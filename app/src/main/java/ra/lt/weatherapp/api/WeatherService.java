package ra.lt.weatherapp.api;

import io.reactivex.Single;
import ra.lt.weatherapp.api.response.WeatherResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {
    @GET("forecast")
    Single<WeatherResponse> getWeather(@Query("appid") String apiKey,
                                       @Query("units") String units,
                                       @Query("lon") double longitude,
                                       @Query("lat") double latitude);
}