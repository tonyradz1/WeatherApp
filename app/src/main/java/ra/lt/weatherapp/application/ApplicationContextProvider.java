package ra.lt.weatherapp.application;

import android.content.Context;

public interface ApplicationContextProvider {
    Context getAppContext();
}
