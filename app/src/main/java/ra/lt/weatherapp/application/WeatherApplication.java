package ra.lt.weatherapp.application;

import android.app.Application;
import android.content.Context;

public class WeatherApplication extends Application implements ApplicationContextProvider {
    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        component.inject(this);
    }

    @Override
    public Context getAppContext() {
        return this;
    }

    public static ApplicationComponent getComponent(Context context) {
        return ((WeatherApplication) context.getApplicationContext()).component;
    }
}
