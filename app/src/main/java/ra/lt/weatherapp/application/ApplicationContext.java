package ra.lt.weatherapp.application;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
