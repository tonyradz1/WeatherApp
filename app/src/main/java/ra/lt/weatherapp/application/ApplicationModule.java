package ra.lt.weatherapp.application;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Context context;

    public ApplicationModule(ApplicationContextProvider contextProvider) {
        this.context = contextProvider.getAppContext();
    }

    @Provides
    @ApplicationContext
    public Context context() {
        return context;
    }
}