package ra.lt.weatherapp.application;

import android.app.Fragment;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ra.lt.weatherapp.utils.imageloader.ImageLoaderModule;
import ra.lt.weatherapp.utils.network.NetworkModule;
import ra.lt.weatherapp.utils.repository.RepositoryModule;
import ra.lt.weatherapp.utils.rx.RxModule;
import ra.lt.weatherapp.weather.WeatherComponent;
import ra.lt.weatherapp.weather.WeatherModule;

@Singleton
@Component(modules = {ApplicationModule.class, RxModule.class,
        NetworkModule.class, ImageLoaderModule.class, RepositoryModule.class})
public interface ApplicationComponent {
    void inject(Context applicationContext);

    void inject(Fragment fragmentContext);

    WeatherComponent plus(WeatherModule module);
}