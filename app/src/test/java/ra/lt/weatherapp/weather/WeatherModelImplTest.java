package ra.lt.weatherapp.weather;

import android.location.Location;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import ra.lt.weatherapp.api.WeatherService;
import ra.lt.weatherapp.api.response.WeatherResponse;
import ra.lt.weatherapp.utils.location.LocationService;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WeatherModelImplTest {
    private WeatherModel model;
    private WeatherRepository weatherRepository;
    private WeatherService weatherService;
    private LocationService locationService;

    @Before
    public void setUp() {
        weatherRepository = mock(WeatherRepository.class);
        weatherService = mock(WeatherService.class);
        locationService = mock(LocationService.class);
        model = new WeatherModelImpl(weatherService, locationService, weatherRepository);
    }

    @Test
    public void testOnGetLocation_modelReturnsLocationFromLocationService() {
        Single<Location> expected = Single.just(mock(Location.class));
        when(locationService.getLocation()).thenReturn(expected);

        Single<Location> actual = model.getLocation();

        verify(locationService).getLocation();
        assertEquals("Expected and actual are different", expected, actual);
    }

    @Test
    public void testSaveWeather_modelSavesWeatherToWeatherRepository() {
        List<Weather> weatherList = new ArrayList<>();
        Weather weather = mock(Weather.class);
        weatherList.add(weather);
        Single<List<Weather>> expected = Single.just(weatherList);
        when(weatherRepository.saveWeather(weatherList)).thenReturn(expected);

        Single<List<Weather>> actual = model.saveWeather(weatherList);

        verify(weatherRepository).saveWeather(weatherList);
        assertEquals("Expected and actual are different", expected, actual);
    }

    @Test
    public void testGetSavedWeather_modelReturnsOfflineWeatherFromWeatherRepository() {
        Weather weather = mock(Weather.class);
        Single<Weather> expected = Single.just(weather);
        when(weatherRepository.getCurrentWeather()).thenReturn(expected);

        Single<Weather> actual = model.getSavedWeather();

        verify(weatherRepository).getCurrentWeather();
        assertEquals("Expected and actual are different", expected, actual);
    }

    @Test
    public void testGetWeather_modelReturnsOnlineWeatherFromWeatherService() {
        Location location = mock(Location.class);
        WeatherResponse weatherResponse = mock(WeatherResponse.class);
        Single<WeatherResponse> expected = Single.just(weatherResponse);
        when(weatherService.getWeather(anyString(), anyString(),
                anyDouble(), anyDouble())).thenReturn(expected);

        Single<WeatherResponse> actual = model.fetchWeather(location);

        verify(weatherService).getWeather(anyString(), anyString(), anyDouble(), anyDouble());
        assertEquals("Expected and actual are different", expected, actual);
    }
}