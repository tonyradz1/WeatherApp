package ra.lt.weatherapp.weather;

import android.location.Location;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import ra.lt.weatherapp.api.response.MainWeatherDetails;
import ra.lt.weatherapp.api.response.ResponseWeather;
import ra.lt.weatherapp.api.response.WeatherDetails;
import ra.lt.weatherapp.api.response.WeatherResponse;
import ra.lt.weatherapp.api.response.Wind;
import ra.lt.weatherapp.utils.date.DateFormatter;
import ra.lt.weatherapp.utils.location.LocationHelper;
import ra.lt.weatherapp.utils.messageshower.MessageShower;
import ra.lt.weatherapp.utils.network.ConnectionChecker;
import ra.lt.weatherapp.utils.permission.PermissionHelper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WeatherPresenterImplTest {
    private static final String TEST_STRING = "test";
    private static final float TEST_FLOAT = 0f;
    private static final int TEST_INT = 0;
    private WeatherPresenterImpl presenter;
    private WeatherView view;
    private WeatherModel model;
    private ConnectionChecker connectionChecker;
    private MessageShower messageShower;
    private PermissionHelper permissionHelper;
    private DateFormatter dateFormatter;
    private Scheduler mainThreadScheduler;
    private LocationHelper locationHelper;

    @Before
    public void setUp() {
        model = mock(WeatherModel.class);
        view = mock(WeatherView.class);
        connectionChecker = mock(ConnectionChecker.class);
        messageShower = mock(MessageShower.class);
        permissionHelper = mock(PermissionHelper.class);
        locationHelper = mock(LocationHelper.class);
        dateFormatter = mock(DateFormatter.class);
        mainThreadScheduler = Schedulers.trampoline();
        presenter = new WeatherPresenterImpl(model, connectionChecker, locationHelper,
                messageShower, permissionHelper, dateFormatter, mainThreadScheduler);
    }

    @Test
    public void testOnLoad_viewAsksForLocationPermissionWhenPermissionIsNotGranted() {
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(false);
        presenter.takeView(view);

        presenter.onLoad();

        verify(view).askLocationPermission(anyInt());
    }

    @Test
    public void testOnLoad_viewDontAskForLocationPermissionWhenPermissionGranted() {
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(model.getSavedWeather()).thenReturn(Single.just(new Weather()));
        presenter.takeView(view);

        presenter.onLoad();

        verify(view, never()).askLocationPermission(anyInt());
        verify(model, never()).getLocation();
        verify(model, never()).fetchWeather(any(Location.class));
    }

    @Test
    public void testOnLoad_modelReturnsOfflineDataWhenOffline() {
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(connectionChecker.isOnline()).thenReturn(false);
        when(model.getSavedWeather()).thenReturn(Single.just(new Weather()));
        presenter.takeView(view);

        presenter.onLoad();

        verify(model).getSavedWeather();
        verify(model, never()).getLocation();
        verify(model, never()).fetchWeather(any(Location.class));
    }

    @Test
    public void testOnLoad_viewShowsOfflineWeatherWhenOffline() {
        Weather weather = new Weather();
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(connectionChecker.isOnline()).thenReturn(false);
        when(model.getSavedWeather()).thenReturn(Single.just(weather));
        presenter.takeView(view);

        presenter.onLoad();

        verify(model).getSavedWeather();
        verify(view).showWeather(weather);
        verify(model, never()).getLocation();
        verify(model, never()).fetchWeather(any(Location.class));
    }

    @Test
    public void testOnLoad_modelReturnsLocationWhenLocationEnabled() {
        Location location = mock(Location.class);
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(connectionChecker.isOnline()).thenReturn(true);
        when(locationHelper.isLocationEnabled()).thenReturn(true);
        when(model.getLocation()).thenReturn(Single.just(location));
        when(model.fetchWeather(location)).thenReturn(Single.just(new WeatherResponse()));
        presenter.takeView(view);

        presenter.onLoad();

        verify(model).getLocation();
        verify(model, never()).getSavedWeather();
    }

    @Test
    public void testOnLoad_modelFetchWeatherWhenLocationRetrieved() {
        Location location = mock(Location.class);
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(connectionChecker.isOnline()).thenReturn(true);
        when(locationHelper.isLocationEnabled()).thenReturn(true);
        when(model.getLocation()).thenReturn(Single.just(location));
        when(model.fetchWeather(location)).thenReturn(Single.just(new WeatherResponse()));
        presenter.takeView(view);

        presenter.onLoad();

        verify(model).getLocation();
        verify(model).fetchWeather(location);
        verify(model, never()).getSavedWeather();
    }

    @Test
    public void testOnLoad_modelReturnsOfflineDataWhenOnlineFetchingFailed() {
        Location location = mock(Location.class);
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(connectionChecker.isOnline()).thenReturn(true);
        when(locationHelper.isLocationEnabled()).thenReturn(true);
        when(model.getLocation()).thenReturn(Single.just(location));
        when(model.fetchWeather(location)).thenReturn(
                Single.<WeatherResponse>error(new Throwable()));
        when(model.getSavedWeather()).thenReturn(Single.just(new Weather()));

        presenter.takeView(view);

        presenter.onLoad();

        verify(model).getLocation();
        verify(model).fetchWeather(location);
        verify(model).getSavedWeather();
        verify(model, never()).saveWeather(ArgumentMatchers.<Weather>anyList());
    }

    @Test
    public void testOnLoad_viewShowsWeatherWhenFetched() {
        WeatherResponse weatherResponse = getWeatherResponse();
        List<Weather> weatherList = new ArrayList<>();
        weatherList.add(mock(Weather.class));
        Location location = mock(Location.class);
        when(permissionHelper.isLocationPermissionGranted()).thenReturn(true);
        when(connectionChecker.isOnline()).thenReturn(true);
        when(locationHelper.isLocationEnabled()).thenReturn(true);
        when(model.getLocation()).thenReturn(Single.just(location));
        when(model.fetchWeather(location)).thenReturn(Single.just(weatherResponse));
        when(model.saveWeather(ArgumentMatchers.<Weather>anyList()))
                .thenReturn(Single.just(weatherList));
        presenter.takeView(view);

        presenter.onLoad();

        verify(view).showWeather(any(Weather.class));
        verify(model).getLocation();
        verify(model, never()).getSavedWeather();
    }

    private WeatherResponse getWeatherResponse() {
        WeatherResponse weatherResponse = new WeatherResponse();
        List<ResponseWeather> responseWeatherList = new ArrayList<>();
        ResponseWeather responseWeather = new ResponseWeather();

        Wind wind = new Wind();
        wind.degree = TEST_FLOAT;
        wind.speed = TEST_FLOAT;

        MainWeatherDetails mainWeatherDetails = new MainWeatherDetails();
        mainWeatherDetails.temperature = TEST_FLOAT;

        List<WeatherDetails> detailsArrayList = new ArrayList<>();
        WeatherDetails weatherDetails = new WeatherDetails();
        weatherDetails.description = TEST_STRING;
        weatherDetails.icon = TEST_STRING;

        detailsArrayList.add(weatherDetails);

        responseWeather.wind = wind;
        responseWeather.mainWeatherDetails = mainWeatherDetails;
        responseWeather.weatherDate = TEST_INT;
        responseWeather.weatherDetails = detailsArrayList;

        responseWeatherList.add(responseWeather);
        weatherResponse.responseWeatherList = responseWeatherList;
        return weatherResponse;
    }
}
